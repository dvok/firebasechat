package com.dvok.firebasechat;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener {

    private static final String DEFAULT_USERNAME = "Default user";
    /**
     * Starting point for Firebase Database operations.
     */
    private DatabaseReference _firebaseDB;

    /**
     * Default provider of Firebase data to RecylcerView.
     */
    private FirebaseRecyclerAdapter<ChatMessage, FirechatMsgViewHolder> _firebaseAdapter;

    /**
     * Recycler view (improved list view) for chat messages.
     */
    private RecyclerView _messageRecyclerView;

    /**
     * Provide RecyclerView with functionality similar to ListView.
     */
    private LinearLayoutManager _linearLayoutManager;

    /**
     * Indicator of chat update process.
     */
    private ProgressBar _progressBar;

    private Button _btnSend;
    private EditText _etMessageText;
    private String _photoUrl;
    private String _username;

    /**
     * Entry point for Firebase authorization process.
     */
    private FirebaseAuth _firebaseAuth;

    /**
     * User's profile information in Firebase project's user database.
     */
    private FirebaseUser _firebaseUser;

    /**
     * Entry point for Google Play services integration.
     */
    private GoogleApiClient _googleApiClient;

    /**
     * Entru point for Firebase analytics services.
     */
    private FirebaseAnalytics _firebaseAnalytics;

    public void onSendButtonClick(View view) {
        ChatMessage newMessage = new ChatMessage(
                _etMessageText.getText().toString(),
                _username,
                _photoUrl
        );

        _firebaseDB.child("messages").push().setValue(newMessage);

        Bundle bundle = new Bundle();
        bundle.putInt("MESSAGE_LENGTH", _etMessageText.getText().length());
        _firebaseAnalytics.logEvent("MESSAGE_SENT", bundle);

        _etMessageText.setText("");


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Google Play Services connection error!", Toast.LENGTH_SHORT).show();
    }

    /**
     * Message view databinding.
     */
    public static class FirechatMsgViewHolder extends RecyclerView.ViewHolder {
        public TextView msgTextView;
        public TextView userTextView;
        public CircleImageView userImageView;

        public FirechatMsgViewHolder(View v) {
            super(v);
            msgTextView = (TextView) itemView.findViewById(R.id.msgTextView);
            userTextView = (TextView) itemView.findViewById(R.id.userTextView);
            userImageView = (CircleImageView) itemView.findViewById(R.id.userImageView);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUIElements();
        initFirebase();
        initAuth();
        initAnalytics();
    }

    private void initAnalytics() {
        _firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //Sets whether analytics collection is enabled for this app on this device.
        _firebaseAnalytics.setAnalyticsCollectionEnabled(true);

        //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
        _firebaseAnalytics.setMinimumSessionDuration(20000);

        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        _firebaseAnalytics.setSessionTimeoutDuration(500);

        if (isEmulator())
            _firebaseAnalytics.setUserProperty("uses_emulator", "true");


    }

    private static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || "goldfish".equals(Build.HARDWARE)
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    private void initAuth() {
        _googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        _firebaseAuth = FirebaseAuth.getInstance();
        _firebaseUser = _firebaseAuth.getCurrentUser();

        if (_firebaseUser == null) {
            startActivity(new Intent(this, AuthorizationActivity.class));
            finish();
            return;
        } else {


            _username = _firebaseUser.getDisplayName();
            if (_firebaseUser.getPhotoUrl() != null) {
                _photoUrl = _firebaseUser.getPhotoUrl().toString();
            }
        }
    }

    /**
     * Initialize UI elements.
     */
    private void initUIElements() {
        //set up refereneces to UI
        _progressBar = (ProgressBar) findViewById(R.id.progressBar);
        _messageRecyclerView = (RecyclerView) findViewById(R.id.messageRecyclerView);


        _linearLayoutManager = new LinearLayoutManager(this);

        //make recycler append new views starting from the bottom of the list
        _linearLayoutManager.setStackFromEnd(true);
        _messageRecyclerView.setLayoutManager(_linearLayoutManager);

        _etMessageText = (EditText) findViewById(R.id.msgEditText);
        _btnSend = (Button) findViewById(R.id.sendButton);

    }

    /**
     * Initialize Firebase database reference and adapter.
     */
    private void initFirebase() {
        _firebaseDB = FirebaseDatabase.getInstance().getReference();
        _firebaseAdapter = new FirebaseRecyclerAdapter<ChatMessage,
                FirechatMsgViewHolder>(
                ChatMessage.class,
                R.layout.chat_message,
                FirechatMsgViewHolder.class,
                _firebaseDB.child("messages")) {

            @Override
            protected void populateViewHolder(FirechatMsgViewHolder viewHolder, ChatMessage friendlyMessage, int position) {
                _progressBar.setVisibility(ProgressBar.INVISIBLE);
                viewHolder.msgTextView.setText(friendlyMessage.getText());
                viewHolder.userTextView.setText(friendlyMessage.getName());
                if (friendlyMessage.getPhotoUrl() == null) {
                    viewHolder.userImageView
                            .setImageDrawable(ContextCompat
                                    .getDrawable(MainActivity.this,
                                            R.drawable.ic_account_circle_black_36dp));
                } else {
                    Glide.with(MainActivity.this)
                            .load(friendlyMessage.getPhotoUrl())
                            .into(viewHolder.userImageView);
                }
            }
        };

        _firebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int chatMessageCount = _firebaseAdapter.getItemCount();
                int lastVisiblePosition = _linearLayoutManager.findLastVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (chatMessageCount - 1)
                                && (lastVisiblePosition == (positionStart - 1))
                        )
                        ) {
                    _messageRecyclerView.scrollToPosition(positionStart);
                }
            }
        });
        _messageRecyclerView.setAdapter(_firebaseAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                return signOut();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean signOut() {
        _firebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(_googleApiClient);
        _username = DEFAULT_USERNAME;
        startActivity(new Intent(this, AuthorizationActivity.class));
        return true;
    }
}


