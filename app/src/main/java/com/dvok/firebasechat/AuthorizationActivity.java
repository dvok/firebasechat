package com.dvok.firebasechat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class AuthorizationActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    /**
     * Request code for Google Sign In Activity
     */
    private static final int RC_SIGN_IN = 9001;

    private SignInButton _btnSignIn;

    /**
     * Entry point for Firebase authorization process.
     */
    private FirebaseAuth _firebaseAuth;

    /**
     * Entry point for Google Play services integration.
     */
    private GoogleApiClient _googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);

        initUIElements();
        initAuth();
    }

    /**
     * Initialize authorization objects.
     */
    private void initAuth() {
        _firebaseAuth = FirebaseAuth.getInstance();

        //configuration object for Sign In Api
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))//specifies that an ID token for authenticated users is requested
                .requestEmail() //specifies that email info is requested
                .build();

        _googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)    //enables automatic lifecycle management
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    /**
     * Initialize UI elements.
     */
    private void initUIElements() {
        _btnSignIn = (SignInButton) findViewById(R.id.sign_in_button);
        _btnSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                Authorize();
                break;
        }
    }

    /**
     * Start Google Sign In Flow
     */
    private void Authorize() {
        //get an Intent to start the Google Sign In flow
        Intent authorizeIntent = Auth.GoogleSignInApi.getSignInIntent(_googleApiClient);
        startActivityForResult(authorizeIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            //extract sign in result from the intent
            GoogleSignInResult signInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (signInResult.isSuccess()) {
                //get Google account information
                GoogleSignInAccount account = signInResult.getSignInAccount();
                firebaseAuthWithGoogleAccount(account);
            } else {
                Toast.makeText(this, "Google Sign In failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Sign into Firebase using credentials from Google Account
     * @param account
     */
    private void firebaseAuthWithGoogleAccount(GoogleSignInAccount account) {
        // get a credential from the google account that the Firebase Authentication server can use to authenticate a user
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        _firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(AuthorizationActivity.this, "Authentication failed!", Toast.LENGTH_SHORT).show();
                        } else {
                            startActivity(new Intent(AuthorizationActivity.this, MainActivity.class));
                            finish();
                        }
                    }
                });
    }

    /**
     * Connection failed handler.
     *
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Google Play Services connection error.", Toast.LENGTH_SHORT).show();
    }
}
